module Main where

import Lib
import Lib.Branch
import Lib.Backpack
import Lib.Approx

main :: IO ()
main = do
  run_tests branch_tests
  print $ branch first_backpack
  print $ branch_and_bound first_backpack
  print $ dynamic_solve first_backpack
  print $ branch second_backpack
  print $ branch_and_bound second_backpack
  print $ dynamic_solve second_backpack
