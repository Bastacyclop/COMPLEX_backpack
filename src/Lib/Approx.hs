module Lib.Approx
  ( Result(..),
    dynamic_solve
  ) where

import Lib.Backpack
import qualified Data.Vector as V

data Result = Result { best_items :: BoolStack,
                       best_utility :: Int,
                       best_weight :: Int } deriving (Show)

empty_result :: Result
empty_result = Result { best_items = empty,
                        best_utility = 0,
                        best_weight = 0 }

vsplit :: V.Vector a -> Maybe (a, V.Vector a)
vsplit v = if V.null v then Nothing else Just (V.head v, V.tail v)

u_max :: Backpack Int -> Int
u_max =
  let f Nothing = 0
      f (Just (i, is)) = V.foldl (\ v i' -> max v (utility i')) (utility i) is
  in f . vsplit . items

compute_v_max :: Backpack Int -> Int
compute_v_max b = V.length (items b) * u_max b

data Cell = Infeasible | Feasible Int deriving (Show, Eq)

type Row = V.Vector Cell

cell_add :: Int -> Cell -> Cell
cell_add x c = case c of
  Infeasible -> Infeasible
  Feasible y -> Feasible $ x + y

cell_min :: Cell -> Cell -> Cell
cell_min a b = case (a, b) of
  (Infeasible, _) -> b
  (_, Infeasible) -> a
  (Feasible x, Feasible y) -> Feasible (min x y)

first_row :: Item Int -> Int -> Row
first_row item v_max =
  let p 0 = Feasible 0
      p v = if v == (utility item) then Feasible (weight item) else Infeasible
  in V.generate v_max p

next_row :: Item Int -> Row -> Int -> Row
next_row item prev_row v_max =
  let u = utility item
      p v = if u <= v
            then let p_with = cell_add (weight item) (prev_row V.! (v - u))
                 in cell_min (prev_row V.! v) p_with
            else prev_row V.! v
  in V.generate v_max p

traceback :: Int -> Cell -> [Row] -> V.Vector (Item Int) -> BoolStack -> BoolStack
traceback v w prs items item_stack =
  if null prs
  then let item = V.last items
       in stack_reverse (if w == Feasible (weight item)
                         then push True item_stack
                         else push False item_stack)
  else let pr = head prs
           prs' = tail prs
           item = V.last items
           items' = V.init items
           pw = pr V.! v
       in if w == pw then traceback v pw prs' items' (push False item_stack)
          else let v' = v - utility item
                   w' = pr V.! v'
               in traceback v' w' prs' items' (push True item_stack)

extract_result :: [Row] -> Backpack Int -> Result
extract_result (lr:rows) b =
  let best (ov, ow) v Infeasible = (ov, ow)
      best (ov, ow) v (Feasible w) = if fits_backpack w b && (succ v) > ov
                                     then (succ v, w) else (ov, ow)
      cell_unwrap (Feasible a) = a
      -- should be 0 < (capacity backpack) :)
      (optimum_v, optimum_w) = V.ifoldl best (0, cell_unwrap (V.head lr)) (V.tail lr)
      bis = traceback optimum_v (Feasible optimum_w) rows (items b) empty
  in Result { best_items = bis,
              best_utility = optimum_v,
              best_weight = optimum_w }

dynamic_solve :: Backpack Int -> Result
dynamic_solve b =
  let v_max = compute_v_max b in
  case vsplit . items $ b of
    Nothing -> empty_result
    Just (i, is) ->
      let fr = first_row i v_max
          push_row prs i' = next_row i' (head prs) v_max : prs
          rows = V.foldl push_row [fr] is
      in extract_result rows b

approx :: Float -> Backpack Int -> Result
approx epsilon b =
  let k = epsilon * (fromIntegral . u_max $ b) /
          (fromIntegral . V.length . items $ b)
      simplify i = i { utility = floor . (/k) . fromIntegral . utility $ i }
      r = dynamic_solve $ b { items = V.map simplify . items $ b }
  in if k < 2.0 then dynamic_solve b
     else r { best_utility = ceiling . (*k) . fromIntegral . best_utility $ r }
