module Lib.Branch
  ( Result,
    branch,
    branch_and_bound,
    branch_tests,
    ) where

import Lib.Backpack

import Data.Int
import qualified Data.Vector as V

data Result a = Result { best_items :: BoolStack,
                         best_utility :: a,
                         best_weight :: a,
                         explored_nodes :: Int64,
                         explored_leafs :: Int32
                       } deriving (Show)

data Node a = Node { node_items :: BoolStack,
                     node_weight :: a,
                     node_utility :: a
                   }

empty_result :: (Integral a) => Result a
empty_result = Result { best_items = empty,
                        best_utility = 0,
                        best_weight = 0,
                        explored_nodes = 0,
                        explored_leafs = 0 }

node_explored :: Result a -> Result a
node_explored r = r { explored_nodes = succ . explored_nodes $ r }

leaf_explored :: Result a -> Result a
leaf_explored r = r { explored_leafs = succ . explored_leafs $ r }

update_result :: (Integral a) => Node a -> Result a -> Result a
update_result n r = if (node_utility n) > (best_utility r)
                    then r { best_items = (node_items n),
                             best_utility = (node_utility n),
                             best_weight = (node_weight n) }
                    else r

node_with :: (Integral a) => Item a -> Node a -> Node a
node_with item n = Node { node_items = push True (node_items n),
                          node_weight = (node_weight n) + (weight item),
                          node_utility = (node_utility n) + (utility item) }

node_without :: Item a -> Node a -> Node a
node_without _item n = n { node_items = push False (node_items n) }

root :: (Integral a) => Node a
root = Node { node_items = empty, node_weight = 0, node_utility = 0 }

type PushNodes a = Node a -> Item a -> Backpack a -> Result a -> [Node a] -> [Node a]

branch_core :: (Integral a) => PushNodes a -> Backpack a -> Result a
branch_core push_nodes b =
  let iter [] r = r
      iter (n:nodes) r =
        (let i = fromIntegral $ size $ node_items n in
         if i == (V.length $ items b)
         then iter nodes . update_result n . leaf_explored
         else let item = (items b) V.! i
              in iter (push_nodes n item b r nodes)
        ) . node_explored $ r
  in iter [root] empty_result

push_nodes :: (Integral a) => PushNodes a
push_nodes n item b r nodes =
  let with = node_with item n
      without = node_without item n
      fits = (\n' -> fits_backpack (node_weight n') b)
  in (if fits with then (with:) else id) . (without:) $ nodes

branch :: (Integral a) => Backpack a -> Result a
branch = branch_core push_nodes

remaining_items :: Node a -> Backpack a -> V.Vector (Item a)
remaining_items n = V.drop (fromIntegral . size . node_items $ n) . items

-- items should be sorted by utility per weight
inferior_bound :: (Integral a) => Node a -> Backpack a -> a
inferior_bound n b =
  let accum (w, u) item =
        let w' = (weight item) + w
            u' = (utility item) + u
        in if w' <= (capacity b) then (w', u') else (w, u)
  in snd . V.foldl (accum) (node_weight n, node_utility n) $ remaining_items n b

vsplit :: V.Vector a -> Maybe (a, V.Vector a)
vsplit v = if V.null v then Nothing else Just (V.head v, V.tail v)

-- items should be sorted by utility per weight
superior_bound :: (Integral a) => Node a -> Backpack a -> a
superior_bound n b =
  let iter (w, u) its =
        case vsplit its of
          Nothing -> u
          Just (item, rest) -> (
            let remaining = (capacity b) - w
                wi = (weight item)
            in if wi <= remaining
               then iter (wi + w, (utility item) + u) rest
               else let r = (fromIntegral remaining) / (fromIntegral wi)
                        u' = (fromIntegral (utility item)) * r + (fromIntegral u)
                    in floor (u' :: Float)
            )
  in iter (node_weight n, node_utility n) (remaining_items n b)

pruning_push :: (Integral a) => PushNodes a
pruning_push n item b r nodes =
  let with = node_with item n
      without = node_without item n
      inf = max (inferior_bound n b) (best_utility r)
      -- FIXME: need to update result with inferior bound and then
      -- we can do: sup <= inf
      prune = (\n' -> (superior_bound n' b) < inf)
      fits = (\n' -> fits_backpack (node_weight n') b)
  in (if prune with || not (fits with) then id else (with:)) .
     (if prune without then id else (without:)) $ nodes

branch_and_bound :: (Integral a) => Backpack a -> Result a
branch_and_bound = branch_core pruning_push . sort_backpack

test_bounds :: (Integral a) => a -> a -> Node a -> Backpack a -> Bool
test_bounds inf sup n b =
  (inf == inferior_bound n b) && (sup == superior_bound n b)

branch_tests :: [Bool]
branch_tests =
  let fb = sort_backpack first_backpack
      sb = sort_backpack second_backpack
  in [
    test_bounds 5 7 root fb,
    test_bounds
      6 6
      (node_without Item { weight = 5, utility = 3 } root)
      fb,
    test_bounds
      35 38
      (node_without Item { weight = 8, utility = 16 } .
       node_with Item { weight = 6, utility = 13 } $ root)
      sb
    ]
