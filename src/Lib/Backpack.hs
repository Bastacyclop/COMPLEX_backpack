module Lib.Backpack
  ( Item(..),
    Backpack(..),
    fits_backpack,
    sort_backpack,
    BoolStack(..),
    empty,
    push,
    stack_reverse,
    first_backpack,
    second_backpack,
    Instances(..),
    random_instances
  ) where

import Data.Int
import Data.List
import qualified Data.Vector as V
import System.Random

data Item a = Item { weight :: a,
                     utility :: a
                   } deriving (Show, Eq)

data Backpack a = Backpack { items :: V.Vector (Item a),
                             capacity :: a
                           } deriving (Show)

fits_backpack :: (Integral a) => a -> Backpack a -> Bool
fits_backpack w = (w <=) . capacity

-- TODO: better sort ?
sort_backpack :: (Integral a) => Backpack a -> Backpack a
sort_backpack b = b { items = V.fromList . utility_sort . V.toList . items $ b }
  where ratio = (\i ->  fromIntegral (weight i) / fromIntegral (utility i))
        utility_sort = sortBy (\a c -> compare (ratio a) (ratio c))

data BoolStack = BoolStack { stack :: [Bool],
                             size :: Int32
                           }

instance Show BoolStack where
  show = show . reverse . stack

empty :: BoolStack
empty = BoolStack { stack = [], size = 0 }

push :: Bool -> BoolStack -> BoolStack
push b s = s { stack = b:(stack s), size = (size s) + 1 }

stack_reverse :: BoolStack -> BoolStack
stack_reverse s = s { stack = reverse . stack $ s }

first_backpack :: Backpack Int
first_backpack = Backpack {
  items = V.fromList [Item { weight = 13,
                             utility = 4 },
                      Item { weight = 6,
                             utility = 2 },
                      Item { weight = 5,
                             utility = 3 }],
  capacity = 20
  }

second_backpack :: Backpack Int
second_backpack = Backpack {
  items = V.fromList [Item { weight = 6,
                             utility = 13 },
                      Item { weight = 8,
                             utility = 16 },
                      Item { weight = 10,
                             utility = 19 },
                      Item { weight = 14,
                             utility = 24 },
                      Item { weight = 2,
                             utility = 3 },
                      Item { weight = 5,
                             utility = 5 }],
  capacity = 20
  }

data Instances = Instances { not_correlated :: [Backpack Int],
                             weakly_correlated :: [Backpack Int],
                             subset_sum :: [Backpack Int] } deriving (Show)

random_backpack :: (Integral a, RandomGen g) => (g -> (Item a, g)) -> Int -> g -> (Backpack a, g)
random_backpack random_item item_count gen =
  let (items, total_weight, gen') = foldl
        (\(is, total_w, g) _ -> let (i, g') = random_item g
                                in (i:is, total_w + (weight i), g'))
        ([], 0, gen)
        (take item_count $ repeat ())
  in (Backpack { items = V.fromList items, capacity = div total_weight 2 }, gen')

not_correlated_random_item :: (RandomGen g) => Int -> g -> (Item Int, g)
not_correlated_random_item r g =
  let (w, g') = randomR (1, r) g
      (u, g'') = randomR (1, r) g'
  in (Item { weight = w, utility = u }, g'')

weakly_correlated_random_item :: (RandomGen g) => Int -> g -> (Item Int, g)
weakly_correlated_random_item r g =
  let (w, g') = randomR (1, r) g
      d = div r 10
      (ud, g'') = randomR (-d, d) g'
  in (Item { weight = w, utility = w + d }, g'')

subset_sum_random_item :: RandomGen g => Int -> g -> (Item Int, g)
subset_sum_random_item r g =
  let (w, g') = randomR (1, r) g
  in (Item { weight = w, utility = w }, g')

random_instances :: (RandomGen g) => Int -> g -> (Instances, g)
random_instances t_max gen =
  let params = [(t*(div t_max 10), r) | t <- [1..10], r <- [100, 1000, 10000]]
      gen_class gen_one g = foldl (\(bs, g') (t, r) ->
                                     let (b, g'') = gen_one t r g'
                                     in (b:bs, g'')
                                  ) ([], g) params
      (nc, gen') = gen_class (\t r g -> random_backpack
                                        (not_correlated_random_item r)
                                        t g) gen
      (wc, gen'') = gen_class (\t r g -> random_backpack
                                         (weakly_correlated_random_item r)
                                         t g) gen'
      (ss, gen''') = gen_class (\t r g -> random_backpack
                                          (subset_sum_random_item r)
                                          t g) gen''
  in (Instances { not_correlated = nc, weakly_correlated = wc, subset_sum = ss },
      gen''')
