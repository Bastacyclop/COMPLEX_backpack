module Lib
    ( run_tests,
    ) where

run_tests :: [Bool] -> IO ()
run_tests tests =
  let iter passed failed output [] =
        let p = (show passed) ++ " passed"
            f = (show failed) ++ " failed"
        in output >> putStrLn ("test results: " ++ p ++ " - " ++ f)
      iter passed failed output (t:ts) =
        let (p, f, status) = if t then (succ passed, failed, "passed")
                             else (passed, succ failed, "failed")
            o = output >> putStrLn ("test " ++ status)
        in iter p f o ts
  in iter (0 :: Int) (0 :: Int) (return ()) tests
